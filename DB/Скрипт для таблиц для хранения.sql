/*������� ������ ����� �� �������� ����*/
If Object_ID('CurrencyDict') is not null
Drop TABLE CurrencyRate
If Object_ID('CurrencyDict') is not null
Drop TABLE CurrencyDict


Create Table CurrencyDict
(
CurrencyName nvarchar(255)
,EngName nvarchar(255)
,Nominal int
,ParentCode nvarchar(10)
,ISO_NumCode nvarchar(6)
,ISO_CharCode nvarchar(5)
,CurrencyID nvarchar(10) not null primary key
)

Create Table CurrencyRate
(
 new_id int primary key identity(1,1)
  ,CurrencyID  nvarchar(10) not null References CurrencyDict(CurrencyID)
 ,RateDate date not null 
 ,Nominal int
 ,RateValue money
)



