
 Declare @Today date = '20200101'
 --Set @Today = CAST('20200622' as Date)
 declare @EndDate date=CAST(GetDate() as Date)
while (@Today<=@EndDate)
begin
/*��������� ���������� ������ �����, ��������� ������� � ��������� CLR ��������*/
 MERGE dbo.CurrencyRate cr 
                            USING (Select CurrencyID,@Today RateDate,RateValue,Nominal from dbo.GetCurrencyRate(convert(nvarchar(10),@Today,104)))
							as crv(CurrencyID,RateDate,RateValue,Nominal) 
							ON cr.CurrencyID = crv.CurrencyID 
                            AND cr.RateDate =crv.RateDate 
                            WHEN MATCHED AND (cr.CurrencyID <>crv.CurrencyID OR cr.RateDate <> crv.RateDate OR 
							cr.RateValue <> crv.RateValue OR cr.Nominal <>crv.Nominal) THEN 
                            Update Set 
                                cr.CurrencyID =crv.CurrencyID, cr.RateDate = crv.RateDate, 
							cr.RateValue = crv.RateValue, cr.Nominal =crv.Nominal
                            WHEN NOT MATCHED THEN
                            insert (CurrencyID,RateDate,RateValue,Nominal) 
                            values (crv.CurrencyID,crv.RateDate,crv.RateValue,crv.Nominal);
							Set @Today =DATEADD(day,1,CAST(@Today as datetime))

End
