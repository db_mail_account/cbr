	 IF  OBJECT_ID('GetCurrencyDict') is not null
		DROP FUNCTION [dbo].[GetCurrencyDict]
		GO
	 IF  OBJECT_ID('GetCurrencyRate') is not null
	   DROP FUNCTION [dbo].[GetCurrencyRate]
		GO

		DROP ASSEMBLY [CBR_CLR]
		GO



Alter DataBase <Your DataBase>
 SET TRUSTWORTHY ON
Create Assembly CBR_CLR from 'c:\DLL\CBR_CLR.dll' with Permission_set = UNSAFE 
GO
     

	 Create Function GetCurrencyDict()
       returns Table (
              CurrencyName nvarchar(255), EngName nvarchar(255),Nominal int,ParentCode nvarchar(10),
            ISO_NumCode nvarchar(6),ISO_CharCode nvarchar(5),CurrencyID nvarchar(10)
       )
       WITH EXECUTE AS CALLER AS
              External name CBR_CLR."CBR_CLR.CLRFunc". GetCurrencyDict
       GO

	  

	   Create Function GetCurrencyRate(@RateDate nvarchar(10))
       returns Table (
              CurrencyID nvarchar(10), RateValue decimal(20,4),Nominal int
       )
        WITH EXECUTE AS CALLER AS
              External name CBR_CLR."CBR_CLR.CLRFunc". GetCurrencyRate
       GO

	  
