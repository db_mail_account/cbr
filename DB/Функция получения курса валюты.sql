/*������� ��������� �������� �����*/
Create Function Currency_Rate(@ISO_CharCode nvarchar(5),@RateDate date)
returns decimal(20,4) as 
begin
	declare @Rate decimal(20,4)
	Set @Rate =(Select RateValue from dbo.CurrencyRate 
	Inner JOIN dbo.CurrencyDict ON CurrencyDict.CurrencyID =dbo.CurrencyRate.CurrencyID Where 
	dbo.CurrencyDict.ISO_CharCode =@ISO_CharCode AND dbo.CurrencyRate.RateDate =@RateDate)
	return @Rate
end

/*�������� ������ �������*/
Select dbo.Currency_Rate('EUR','20200623') as RateCurrency
