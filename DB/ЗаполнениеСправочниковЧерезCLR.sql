/*������ ������� ����� ��������� � ������� ������� � ��������� �� �� �����������*/

 Declare @Today date = Cast(GetDate() as Date)
 
 
/*��������� ���������� ������ �����, ��������� ������� � ��������� CLR ��������*/
 MERGE dbo.CurrencyRate cr 
                            USING (Select CurrencyID,@Today RateDate,RateValue,Nominal from dbo.GetCurrencyRate(convert(nvarchar(10),@Today,104)))
							as crv(CurrencyID,RateDate,RateValue,Nominal) 
							ON cr.CurrencyID = crv.CurrencyID 
                            AND cr.RateDate =crv.RateDate 
                            WHEN MATCHED AND (cr.CurrencyID <>crv.CurrencyID OR cr.RateDate <> crv.RateDate OR 
							cr.RateValue <> crv.RateValue OR cr.Nominal <>crv.Nominal) THEN 
                            Update Set 
                                cr.CurrencyID =crv.CurrencyID, cr.RateDate = crv.RateDate, 
							cr.RateValue = crv.RateValue, cr.Nominal =crv.Nominal
                            WHEN NOT MATCHED THEN
                            insert (CurrencyID,RateDate,RateValue,Nominal) 
                            values (crv.CurrencyID,crv.RateDate,crv.RateValue,crv.Nominal);


/*��������� ����������  �����, ��������� ������� � ��������� CLR ��������*/
	   MERGE dbo.CurrencyDict cd
                            USING(Select CurrencyName,EngName,Nominal,ParentCode
							,ISO_NumCode,ISO_CharCode,CurrencyID from dbo.GetCurrencyDict()) as 
							cdv(CurrencyName,EngName,Nominal,ParentCode
							,ISO_NumCode,ISO_CharCode,CurrencyID) ON cd.CurrencyID = cdv.CurrencyID
                            WHEN MATCHED AND (cd.CurrencyName<>cdv.CurrencyName
							OR cd.EngName<>cdv.EngName OR cd.Nominal<>cdv.Nominal OR cd.ParentCode<>cdv.ParentCode
							OR cd.ISO_NumCode<>cdv.ISO_NumCode OR cd.ISO_CharCode<>cdv.ISO_CharCode OR cd.CurrencyID<>cdv.CurrencyID) THEN 
                            Update Set 
                                cd.CurrencyName=cdv.CurrencyName,cd.EngName=cdv.EngName
							   ,cd.Nominal=cdv.Nominal,cd.ParentCode=cdv.ParentCode,cd.ISO_NumCode=cdv.ISO_NumCode
							   ,cd.ISO_CharCode=cdv.ISO_CharCode,cd.CurrencyID=cdv.CurrencyID
                            WHEN NOT MATCHED THEN
                            insert (CurrencyName,EngName,Nominal,ParentCode
								,ISO_NumCode,ISO_CharCode,CurrencyID) 
                            values (cdv.CurrencyName,cdv.EngName,cdv.Nominal,cdv.ParentCode
								,cdv.ISO_NumCode,cdv.ISO_CharCode,cdv.CurrencyID);




