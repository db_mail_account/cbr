﻿using System;
using System.IO;
using System.Text;
using System.Net;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace USD
    {
        class Program
        {
        /// <summary>
        /// Строка соединения с источником 
        /// </summary>
        private static string cnn = @"Password=your_password;Persist Security Info=True;User ID=your_user;Initial Catalog=Database_Name;Data Source=server_name";
        static void Main(string[] args)
            {
            string actionchar = "";
            while (actionchar != "q")
            {
                
                Console.WriteLine(@"Введите  d  для загрузки справочника  валют.");
                
                Console.WriteLine(@"Введите  r  для загрузки таблицы курсов валют на  дату.");
                Console.WriteLine(@"Введите  v  для получения курса валюты.");
                Console.WriteLine(@"Введите  q  для выхода из программы.");
                actionchar = Console.ReadLine();

                switch (actionchar)
                {
                    case "d":
                        {
                            string url = @"http://www.cbr.ru/scripts/XML_valFull.asp";
                            LoadCurrencyDict(url);
                            Console.WriteLine("Операция завершена \n");
                            break;
                        }
                    case "r":
                        {
                            string url = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=";
                            bool bInput = false;
                            DateTime RateDate =DateTime.Now;
                            while (bInput==false)
                            {
                                Console.WriteLine(@"Введите дату, на которую нужно загружать курс.");
                                bInput = DateTime.TryParse(Console.ReadLine(), out RateDate );
                               

                            }
                            url += RateDate.ToShortDateString();
                            LoadCurrencyRate(url, RateDate);
                            Console.WriteLine("Операция завершена \n");
                            break;
                        }
                    case "v":
                        {
                            DateTime RateDate = DateTime.Now;
                            string ISO_CharCode = "";
                            bool bInput = false;
                           
                                Console.WriteLine(@"Введите буквенный код валюты, для которой нужно получить курс.");
                            ISO_CharCode = Console.ReadLine();
                               
                           
                            while (bInput == false)
                            {
                                Console.WriteLine(@"Введите дату, на которую нужно получить курс.");
                                bInput = DateTime.TryParse(Console.ReadLine(), out RateDate);
                            }
                            decimal val = GetCurrencyRate(ISO_CharCode, RateDate);
                            Console.WriteLine("курс валюты {0} на {1} составляет {2} \n",ISO_CharCode,RateDate.ToShortDateString(),val.ToString() );
                            break;
                        }

                }

            }

            
          
            }

        
        /// <summary>
        /// Загрузить справочник валют
        /// </summary>
        private static void LoadCurrencyDict(string url)
        {
           
            DataSet ds = null;
            string xml = string.Empty;

            //Получить справочник курсов
            xml = RequestXML(url);
            if (xml != "")
                ds = ReadXml(xml);
            SyncCurrencyDict(ds.Tables["Item"]);
            xml = string.Empty;
        }

        /// <summary>
        /// Загрузить курсы валют на дату
        /// </summary>
        /// <param name="RateDate"></param>
        private static void LoadCurrencyRate(string url,DateTime RateDate)
        {
            
            //Получить курсы на заданную дату
            DataSet ds = null;
            string xml = string.Empty;
            xml = RequestXML(url);
            if (xml != "")
                ds = ReadXml(xml);
            SyncCurrencyRate(ds.Tables["Valute"], RateDate);

        }
        /// <summary>
        /// Получить курс валюты на указанную дату
        /// </summary>
        /// <param name="ISO_CharCode"></param>
        /// <param name="RateDate"></param>
        private static decimal  GetCurrencyRate(string ISO_CharCode,DateTime RateDate)
        {
            try
            {
                string sCmd = "Select dbo.Currency_Rate(@ISO_CharCode,@RateDate) as RateCurrency";
                List<SqlParameter> pars = new List<SqlParameter>();
                pars.Add(new SqlParameter("@ISO_CharCode", SqlDbType.NVarChar) { Value = ISO_CharCode });
                pars.Add(new SqlParameter("@RateDate", SqlDbType.Date) { Value =RateDate });
              decimal returnValue= Convert.ToDecimal(sqlDataLayer.sqlQueryHelper.ExecuteScalar(cnn, sCmd, CommandType.Text, pars.ToArray()));
               return returnValue;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Сохранение данных справочника курсов
        /// </summary>
        /// <param name="dt">таблица с сохраняемыми данными</param>
        private static void SyncCurrencyDict(DataTable dt)
        {
            try
            { 
            string @sCmd = @"MERGE dbo.CurrencyDict cd
                            USING (Select @CurrencyName,@EngName,@Nominal,
							@ParentCode,@ISO_NumCode,@ISO_CharCode,@CurrencyID) as 
							cdv(CurrencyName,EngName,Nominal,ParentCode
							,ISO_NumCode,ISO_CharCode,CurrencyID) ON cd.CurrencyID = cdv.CurrencyID
                            WHEN MATCHED AND (cd.CurrencyName<>cdv.CurrencyName
							OR cd.EngName<>cdv.EngName OR cd.Nominal<>cdv.Nominal OR cd.ParentCode<>cdv.ParentCode
							OR cd.ISO_NumCode<>cdv.ISO_NumCode OR cd.ISO_CharCode<>cdv.ISO_CharCode OR cd.CurrencyID<>cdv.CurrencyID) THEN 
                            Update Set 
                                cd.CurrencyName=cdv.CurrencyName,cd.EngName=cdv.EngName
							   ,cd.Nominal=cdv.Nominal,cd.ParentCode=cdv.ParentCode,cd.ISO_NumCode=cdv.ISO_NumCode
							   ,cd.ISO_CharCode=cdv.ISO_CharCode,cd.CurrencyID=cdv.CurrencyID
                            WHEN NOT MATCHED THEN
                            insert (CurrencyName,EngName,Nominal,ParentCode
								,ISO_NumCode,ISO_CharCode,CurrencyID) 
                            values (cdv.CurrencyName,cdv.EngName,cdv.Nominal,cdv.ParentCode
								,cdv.ISO_NumCode,cdv.ISO_CharCode,cdv.CurrencyID);";

                foreach (DataRow item in dt.Rows)
                {
                    List<SqlParameter> pars = new List<SqlParameter>();
                    pars.Add(new SqlParameter("@CurrencyName", SqlDbType.NVarChar) { Value = item["Name"].ToString() });
                    pars.Add(new SqlParameter("@EngName", SqlDbType.NVarChar) { Value = item["EngName"].ToString() });
                    pars.Add(new SqlParameter("@Nominal", SqlDbType.Int) { Value = item["Nominal"].ToString() });
                    pars.Add(new SqlParameter("@ParentCode", SqlDbType.NVarChar) { Value = item["ParentCode"].ToString() });
                    pars.Add(new SqlParameter("@ISO_NumCode", SqlDbType.NVarChar) { Value = item["ISO_Num_Code"].ToString() });
                    pars.Add(new SqlParameter("@ISO_CharCode", SqlDbType.NVarChar) { Value = item["ISO_Char_Code"].ToString() });
                    pars.Add(new SqlParameter("@CurrencyID", SqlDbType.NVarChar) { Value = item["ID"].ToString() });
                    int rowsAffected = sqlDataLayer.sqlQueryHelper.ExecuteNonQuery(cnn, sCmd, CommandType.Text, pars.ToArray());
                    // if(rowsAffected) 
                    //{Что то делаем }
                    pars = null;
                }
            }
            catch
            {
                throw; //Добавить нужную обработку
            }
        }
        /// <summary>
        /// Сохранение полученных данных 
        /// </summary>
        /// <param name="dt">таблица с данными курсов</param>
        /// <param name="RateDate">дата на которую загружены данные</param>
        private static void SyncCurrencyRate(DataTable dt,DateTime RateDate)
        {
            try
            {
                string @sCmd = @"  MERGE dbo.CurrencyRate cr 
                            USING (Select @CurrencyID,@RateDate,@RateValue,@Nominal) as crv(CurrencyID,RateDate,RateValue,Nominal) 
							ON cr.CurrencyID = crv.CurrencyID 
                            AND cr.RateDate =crv.RateDate 
                            WHEN MATCHED AND (cr.CurrencyID <>crv.CurrencyID OR cr.RateDate <> crv.RateDate OR 
							cr.RateValue <> crv.RateValue OR cr.Nominal <>crv.Nominal) THEN 
                            Update Set 
                                cr.CurrencyID =crv.CurrencyID, cr.RateDate = crv.RateDate, 
							cr.RateValue = crv.RateValue, cr.Nominal =crv.Nominal
                            WHEN NOT MATCHED THEN
                            insert (CurrencyID,RateDate,RateValue,Nominal) 
                            values (crv.CurrencyID,crv.RateDate,crv.RateValue,crv.Nominal);";

                foreach (DataRow item in dt.Rows)
                {
                    List<SqlParameter> pars = new List<SqlParameter>();
                    pars.Add(new SqlParameter("@CurrencyID", SqlDbType.NVarChar) { Value = item["ID"].ToString() });
                    pars.Add(new SqlParameter("@RateDate", SqlDbType.Date) { Value = RateDate });
                    pars.Add(new SqlParameter("@RateValue", SqlDbType.Decimal) { Value = Convert.ToDecimal(item["Value"]) });
                    pars.Add(new SqlParameter("@Nominal", SqlDbType.Int) { Value = item["Nominal"].ToString() });
                    int rowsAffected = sqlDataLayer.sqlQueryHelper.ExecuteNonQuery(cnn, sCmd, CommandType.Text, pars.ToArray());
                    // if(rowsAffected) 
                    //{Что то делаем }
                    pars = null;
                }
            }
            catch
            {
                throw; //Добавить нужную обработку
            }
        }



        /// <summary>
        /// Получение xml по url адресу
        /// </summary>
        /// <param name="url">Адрес сервиса с данными</param>
        /// <returns></returns>

        private static string RequestXML(string url)
        {
            try
            {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                StreamReader myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream(), Encoding.Default);
                return myStreamReader.ReadToEnd();
            }
            catch
            {
                throw; //Добавить нужную обработку
            }
        }

        /// <summary>
        /// Чтение xml в набор данных
        /// </summary>
        /// <param name="rawxml">Строка с XML данными</param>
        /// <returns></returns>
        private static DataSet ReadXml(string rawxml)
        {
            try
            {
                byte[] xmlBytes = Encoding.Default.GetBytes(rawxml);
                DataSet ds = new DataSet();
                using (MemoryStream ms = new MemoryStream(xmlBytes))
                {
                    ds.ReadXml(ms);
                }
                return ds;
            }
            catch
            {
                throw;//Добавить нужную обработку
            }
        }
    }


    }


