﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace sqlDataLayer
{
    public class sqlQueryHelper
    {


        public static SqlDataReader ExecuteReader(String connectionString, String commandText,
            CommandType commandType, params SqlParameter[] parameters)
        {
            
            SqlConnection conn = new SqlConnection(connectionString);

            using (SqlCommand cmd = new SqlCommand(commandText, conn))
            {
                cmd.CommandType = commandType;
                cmd.Parameters.AddRange(parameters);

                conn.Open();
                // When using CommandBehavior.CloseConnection, the connection will be closed when the 
                // IDataReader is closed.
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return reader;
            }
        }

        public static DataTable executeSelectQuery(String cnn, string strQuery)
        {
            DataTable dtRes = new DataTable();
            
            using (SqlConnection conn = new SqlConnection(cnn))
            {
                //Открыть соединение
                conn.Open();
                using (SqlCommand u_selectCommand = new System.Data.SqlClient.SqlCommand(strQuery, conn))
                {
                    using (SqlDataAdapter u_DaSql = new SqlDataAdapter(u_selectCommand))
                    {
                        u_DaSql.Fill(dtRes);
                    }
                }
                return dtRes;
            }

        }

        public static Int32 executeCommand(String cnn, String sCmd)
        {
           

            using (SqlConnection conn = new SqlConnection(cnn))
            {
               
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sCmd, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    return cmd.ExecuteNonQuery();

                }
            }


        }
        public static Object ExecuteScalar(String connectionString, String commandText,
           CommandType commandType, params SqlParameter[] parameters)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(commandText, conn))
                    {
                        cmd.CommandType = commandType;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        return cmd.ExecuteScalar();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        public static Int32 ExecuteNonQuery(String connectionString, String commandText,
           CommandType commandType, params SqlParameter[] parameters)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(commandText, conn))
                    {
                        // There're three command types: StoredProcedure, Text, TableDirect. The TableDirect 
                        // type is only for OLE DB.  
                        cmd.CommandType = commandType;
                        cmd.Parameters.AddRange(parameters);

                        conn.Open();
                        return cmd.ExecuteNonQuery();
                    }
                }
            }
            catch
            {
               

                throw;
            }
        }
        //private async Task<DataTable> ExecuteQueryInternalAsync(string commandText, CommandType commandType, SqlConnection sqlConnection, SqlTransaction transaction,params SqlParameter[] parameters)
        //{
        //    using (SqlCommand cmd = new SqlCommand(commandText, sqlConnection) { CommandType = commandType, CommandTimeout = this.config.MainConnectionTimeoutInSeconds })
        //    {
        //        if (transaction != null)
        //            cmd.Transaction = transaction;
        //        if (parameters!=null)
        //        cmd.LoadParams(parameters);

        //        if (sqlConnection.State == ConnectionState.Closed)
        //            await sqlConnection.OpenAsync();

        //        var datatable = await cmd.ExecuteAndCreateDataTableAsync();
        //        return datatable;
        //    }
        //}
       
        // public static SqlParameter CreateParameter(string ParName, SqlDbType ParType)
        //{
        //    SqlParameter par = new System.Data.SqlClient.SqlParameter();
        //    par.ParameterName = ParName;
        //    par.SqlDbType = ParType;

        //    return par;
        //}
      

        public static void SetValue(SqlParameter sqlPar, string formValue, object CellValue)
        {
            if (formValue == CellValue.ToString())
            {
                SetValueByType(sqlPar, CellValue.ToString());
            }
            else
            {
                SetValueByType(sqlPar, formValue);
            }

        }
        public static void SetValue(SqlParameter sqlPar, string formValue)
        {
           
                SetValueByType(sqlPar, formValue);
            

        }

        private static void SetValueByType(SqlParameter sqlPar, string value)
        {
            switch (sqlPar.SqlDbType)
            {
                case (SqlDbType.Int):
                    {
                        sqlPar.Value = Convert.ToInt32(value);
                        break;
                    }
                case (SqlDbType.NVarChar):
                    {
                        sqlPar.Value = value;
                        break;
                    }
                case (SqlDbType.Decimal):
                    {
                        sqlPar.Value = Convert.ToDecimal(value);
                        break;
                    }

            }

        }

    }
    public static class extensions
    {
        public async static Task<DataTable> ExecuteAndCreateDataTableAsync(this SqlCommand cmd)
        {
            using (var reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false))
            {
                var dataTable = reader.CreateTableSchema();
                while (await reader.ReadAsync().ConfigureAwait(false))
                {
                    var dataRow = dataTable.NewRow();
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        dataRow[i] = reader[i];
                    }
                    dataTable.Rows.Add(dataRow);
                }
                return dataTable;

            }
        }
        public static void LoadParams(this SqlCommand cmd, params SqlParameter[] parameters)
        {
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    if (parameter != null)
                    {
                        if (parameter.Value == null)
                            parameter.Value = DBNull.Value;

                        cmd.Parameters.Add(parameter);
                    }
                }
            }
        }


        public static DataTable CreateTableSchema(this SqlDataReader reader)
        {
            DataTable schema = reader.GetSchemaTable();
            DataTable dataTable = new DataTable();
            if (schema != null)
            {
                foreach (DataRow drow in schema.Rows)
                {
                    string columnName = System.Convert.ToString(drow["ColumnName"]);
                    DataColumn column = new DataColumn(columnName, (Type)(drow["DataType"]));
                    dataTable.Columns.Add(column);
                }
            }
            return dataTable;
        }
       
    }
}
