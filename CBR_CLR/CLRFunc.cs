﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Server;
using System.Data.SqlTypes;
using System.Data;

namespace CBR_CLR
{
    /// <summary>
    /// Класс в котором соберем CLR функции
    /// </summary>
    public class CLRFunc
    {
        /// <summary>
        /// Функция получения справочника курсов валют
        /// </summary>
        /// <returns></returns>
        [SqlFunction(
      DataAccess = DataAccessKind.None,
      FillRowMethodName = "FillCurrencyDict",
      TableDefinition = "CurrencyName nvarchar(255), EngName nvarchar(255),Nominal int,ParentCode nvarchar(10)," +
            "ISO_NumCode nvarchar(6),ISO_CharCode nvarchar(5),CurrencyID nvarchar(10)")]
        static public IEnumerator GetCurrencyDict()
        {
            string url = @"http://www.cbr.ru/scripts/XML_valFull.asp";
            string xml = string.Empty;
            pagehelper p = new pagehelper();
            DataSet ds = null;

            //Получить справочник курсов
            xml = p.RequestXML(url);
            //Перевести в dataset
            if (xml != "")
                ds = p.ReadXml(xml);

            //Более простой вариант получения данных
            //DataSet ds = new DataSet();
            //ds.ReadXml(url);
            //xml = string.Empty;

            //Вернуть серверу 
            foreach (DataRow item_row in ds.Tables["Item"].Rows)
            {
                yield return item_row;
            }
        }
        
   public static void FillCurrencyDict(object result, out SqlString CurrencyName,
  out SqlString EngName, out SqlInt32 Nominal, out SqlString ParentCode, out SqlString ISO_NumCode, out SqlString ISO_CharCode,out SqlString CurrencyID)
        {
            DataRow item_row = (DataRow)result;
            CurrencyName = item_row["Name"].ToString();
            EngName = item_row["EngName"].ToString();
            Nominal = Convert.ToInt32(item_row["Nominal"]);
            ParentCode = item_row["ParentCode"].ToString();
            ISO_NumCode = item_row["ISO_Num_Code"].ToString();
            ISO_CharCode = item_row["ISO_Char_Code"].ToString();
            CurrencyID = item_row["ID"].ToString();
        }

        /// <summary>
        /// функция получения таблицы с значениями курсов валют
        /// </summary>
        /// <param name="RateDate">Дата, на которую подгружаются курсы</param>
        /// <returns></returns>
        [SqlFunction(
     DataAccess = DataAccessKind.None,
     FillRowMethodName = "FillCurrencyRate",
     TableDefinition = "CurrencyID nvarchar(10), RateValue decimal(20,4),Nominal int")]
        static public IEnumerator GetCurrencyRate(string RateDate) //Попробовать переделать на DateTime
        {
           
            DataSet ds = null;
            pagehelper p = new pagehelper();
           //Получить курсы на заданную дату
            string url = @"http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + RateDate;
            string xml = p.RequestXML(url);
            if (xml != "")
                ds = p.ReadXml(xml);

            //Более простой вариант получения данных
            //DataSet ds = new DataSet();
            //ds.ReadXml(url);

            //Вернуть серверу
            foreach (DataRow item in ds.Tables["Valute"].Rows)
            {
                yield return item;
            }
        }

        public static void FillCurrencyRate(object result, out SqlString CurrencyID,
  out SqlDecimal RateValue, out SqlInt32 Nominal)
        {
            DataRow item_row = (DataRow)result;
            CurrencyID = item_row["ID"].ToString();
            RateValue = Convert.ToDecimal(item_row["Value"]);
            Nominal = Convert.ToInt32(item_row["Nominal"]);  
        }
    }
}
