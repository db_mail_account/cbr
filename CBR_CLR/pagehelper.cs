﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace CBR_CLR
{
    class pagehelper
    {

        public static bool TrustAllCertificateCallback(object sender,
       X509Certificate cert, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }

        /// <summary>
        /// Получение xml по url адресу
        /// </summary>
        /// <param name="url">Адрес сервиса с данными</param>
        /// <returns></returns>

        public string RequestXML(string url)
        {
            //В примере используем http, проверка сертификата не нужна
           // ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(TrustAllCertificateCallback);
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            StreamReader myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream(), Encoding.Default);
            return myStreamReader.ReadToEnd();
        }
        /// <summary>
        /// Чтение xml в набор данных
        /// </summary>
        /// <param name="rawxml">Строка с XML данными</param>
        /// <returns></returns>
        public DataSet ReadXml(string rawxml)
        {
            byte[] xmlBytes = Encoding.Default.GetBytes(rawxml);
            DataSet ds = new DataSet();
            using (MemoryStream ms = new MemoryStream(xmlBytes))
            {
                ds.ReadXml(ms);
            }
            return ds;
        }
    }
}
